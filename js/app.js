jQuery(document).ready(function ($) {

    /*** VARIABLES GENERALES ***/

    var $window = $(window);
    var $document = $(document);
    var header = $('header');
    var megamenu = $('.megamenu');
    var burger = $('.burger');
    var body = $('body');
    var footer = $('footer');
    var niveau1_li = $('.menu-desktop > li');
    var niveau1_a = $('.menu-desktop > li > a');
    var burger_ul = $('.menu-mobile ul');
    var burger_ul_3e_nv = $('.menu-mobile ul ul');
    var burger_header = $('.burger-header');
    var dropdownMegamenu = $('.dropdown-megamenu');
    var contenuDropdown = $('.dropdown-megamenu .contenu-dropdown');
    var sliderSuccessStories = $('.slider-success-stories');

    /*** FIN VARIABLES GENERALES ***/


    /*** IMPORT DES JS ***/
    function loadScript(path) {
        $.getScript(path, function (data, status, res) {
            if (res.status === 200) {
                var filename = path.replace(/^.*[\\\/]/, '');
                console.log(filename + ' chargé.');
            }
        })
    }


    /*** FIN D'IMPORT DES JS ***/


    /*** UTILITIES ***/
    // wait function
    $.wait = function (ms) {
        var defer = $.Deferred();
        setTimeout(function () {
            defer.resolve();
        }, ms);
        return defer;
    };

    //ScrollTo
    function scrollTo(target, condition) {
        if (condition.length) {
            var scroll = target.offset().top - $('header').height(); // prise en compte du header sticky - si sticky partiel, mettre la partie du header qui sera sticky à la place
            $("html, body").stop().animate({scrollTop: scroll}, 1500);
        }
    }

    // Add smooth scrolling to all links
    $("a[href^=\"#\"]").on('click', function (event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it
            // takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top - 150
            }, 800, function () {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });

    (function (document, history, location) {
        var HISTORY_SUPPORT = !!(history && history.pushState);


        var anchorScrolls = {
            ANCHOR_REGEX: /^#[^ ]+$/,
            OFFSET_HEIGHT_PX: 150,

            /**
             * Establish events, and fix initial scroll position if a hash is provided.
             */
            init: function () {
                this.scrollToCurrent();
                $(window).on('hashchange', $.proxy(this, 'scrollToCurrent'));
                $('body').on('click', 'a', $.proxy(this, 'delegateAnchors'));
            },

            /**
             * Return the offset amount to deduct from the normal scroll position.
             * Modify as appropriate to allow for dynamic calculations
             */
            getFixedOffset: function () {
                return this.OFFSET_HEIGHT_PX;
            },

            /**
             * If the provided href is an anchor which resolves to an element on the
             * page, scroll to it.
             * @param  {String} href
             * @return {Boolean} - Was the href an anchor.
             */
            scrollIfAnchor: function (href, pushToHistory) {
                var match, anchorOffset;

                if (!this.ANCHOR_REGEX.test(href)) {
                    return false;
                }

                match = document.getElementById(href.slice(1));

                if (match) {
                    anchorOffset = $(match).offset().top - this.getFixedOffset();
                    $('html, body').animate({scrollTop: anchorOffset});

                    // Add the state to history as-per normal anchor links
                    if (HISTORY_SUPPORT && pushToHistory) {
                        history.pushState({}, document.title, location.pathname + href);
                    }
                }

                return !!match;
            },

            /**
             * Attempt to scroll to the current location's hash.
             */
            scrollToCurrent: function (e) {
                if (this.scrollIfAnchor(window.location.hash) && e) {
                    e.preventDefault();
                }
            },

            /**
             * If the click event's target was an anchor, fix the scroll position.
             */
            delegateAnchors: function (e) {
                var elem = e.target;

                if (this.scrollIfAnchor(elem.getAttribute('href'), true)) {
                    e.preventDefault();
                }
            }
        };


        $(document).ready($.proxy(anchorScrolls, 'init'));
    })(window.document, window.history, window.location);

    /*** Fin UTILITIES ***/

    /*** GLOBAL ***/
    // Tooltips trigger
    $('[data-toggle="tooltip"]').tooltip();

    // tableau responsive
    $('.wysiwyg table, .main-content table').wrap('<div class="tableau-responsive container"></div>');

    // loader sur les boutons
    $('.btn').on('click', function () {
        if (!$(this).hasClass('no-loader')) {
            if (!$(this).attr('target')) {
                $(this).width($(this).width()); // pour empêcher le changement de taille du bouton
                $(this).addClass('btn-loader');
                $(this).html('<i class="fas fa-circle-notch fa-spin"></i>');
            }
            // ajoutez la class "no-loader" aux boutons que vous souhaitez exclure
        }
    });

    // loader sur le bouton submit en input
    $('input[type="submit"]').on('click', function () {
        if ($(this).parents('form')[0].checkValidity()) {
            $(this).wrap('<span class="input-loader"></span>');
            $(this).parent().append('<div class="msg-envoi"><i class="fas fa-circle-notch fa-spin"></i><span>Veuillez patienter...</span></div>');
        }
    });

    // Scroll to error - gravity forms
    scrollTo($(".formulaire-anchor"), $(".gform_validation_error"), 1500); // ajoutez la classe "formulaire-anchor" dans les réglages du formulaire en back-office

    var navhei = $('.conteneur-logo').height();
    var navheix = navhei + 70;
    document.addEventListener('invalid', function (e) {
        $(e.target).addClass("invalid");

        $('html, body').stop().animate({
            scrollTop: ($($(".invalid")[0]).offset().top - navheix)
        }, 300);

        setTimeout(function () {
            $('.invalid').removeClass('invalid');
        }, 300);
    }, true);

    // Tarte au citron contraignant
    setTimeout(function () {
        if ($("#tarteaucitronAlertBig").css("display") === "block") {
            $("#tarteaucitronRoot").addClass("blur");
        }
    }, 1500);
    $document.on("click", "#tarteaucitronPersonalize", function () {
        $("#tarteaucitronRoot").removeClass("blur");
    });
    $document.on("click", "#tarteaucitronClosePanel", function () {
        window.location.reload();
    });


    /*** FIN GLOBAL ***/

    /*** HEADER ***/

    // Si besoin de modifier le header au scroll, décommenter cette section

    $window.on('scroll', function () {
        var windowPos = $window.scrollTop();
        if ($window.width() > 992) {
            if (windowPos > 10 && !header.hasClass('scrolling')) {
                header.addClass('scrolling');
            } else if (windowPos <= 10) {
                header.removeClass('scrolling');
            }
        }
    });

    $window.on('resize', function () {
        if ($window.width() < 992 && header.hasClass('scrolling')) {
            header.removeClass('scrolling');
        }
    });

    /*** FIN HEADER ***/


    /*** MEGAMENU ***/


    /** Mégamenu au clic **/
    // niveau1_a.on('click', function(event){
    //     event.preventDefault();
    //     if($(this).parent().hasClass('active')){
    //         niveau1_li.removeClass('active');
    //     } else {
    //         niveau1_li.removeClass('active');
    //         $(this).parent().addClass('active');
    //     }
    // });


    /** Mégamenu au hover **/
    niveau1_a.on('mouseover', function () {
        if (!dropdownMegamenu.hasClass('active')) {
            niveau1_li.removeClass('active');
            $(this).parent().addClass('active');
        }
    });
    $('.menu-desktop').on('mouseleave', function () {
        niveau1_li.removeClass('active');
    });

    /** "Popup" carrefour **/

    niveau1_li.each(function (count) {
        $(this).attr('id', 'trigger-' + count);
    });
    contenuDropdown.each(function (count) {
        $(this).attr('id', 'drop-' + count);
    });

    niveau1_a.on('click', function (event) {
        event.preventDefault();
        if ($(this).parent().hasClass('active-drop')) {
            niveau1_li.removeClass('active').removeClass('active-drop');
            dropdownMegamenu.removeClass('active');
            contenuDropdown.removeClass('active');
        } else {
            var id = $(this).parent().attr('id').split('-').pop();
            niveau1_li.removeClass('active').removeClass('active-drop');
            $(this).parent().addClass('active-drop');
            dropdownMegamenu.addClass('active');
            contenuDropdown.removeClass('active');
            $('.contenu-dropdown#drop-' + id).addClass('active');
        }
    });

    $('.close-megamenu').on('click', function () {
        dropdownMegamenu.removeClass('active');
        niveau1_li.removeClass('active-drop');
    });

    // fermeture de tous les dropdowns quand on clique à l'extérieur du menu
    $document.click(function (event) {
        if (!$(event.target).closest('.megamenu').length) {
            if ($window.width() > 991) { // uniquement en desktop
                niveau1_li.removeClass('active').removeClass('active-drop');
                dropdownMegamenu.removeClass('active');
                contenuDropdown.removeClass('active');
            }
        }
    });

    /** Fin "Popup" carrefour **/


    /*** FIN MEGAMENU ***/

    /*** MENU BURGER ***/

    // ouverture/fermeture du burger
    $('.hamburger').on('click', function () {
        $(this).toggleClass('is-active');
        if (burger.hasClass('active')) {
            // fermeture du burger
            burger.removeClass('active');
            burger_ul.removeClass('active');
            burger_header.removeClass('active');
            burger_header.find('.conteneur-rappel-menu').html('<span class="rappel-menu">Nos rubriques</span>');
            body.removeClass('no-scroll');
        } else {
            // ouverture du burger
            burger.addClass('active');
            body.addClass('no-scroll');
        }
    });

    // Ajout de plus à côté des ul contenant des enfants
    burger_ul.each(function () {
        if ($(this).children().length > 0) {
            $(this).parent().append('<span class="plus"></span>');
        }
    });

    // ouverture des dropdowns
    $('.menu-mobile .plus').on('click', function () {
        var nomParent = $(this).siblings('a').html();
        var hrefParent = $(this).siblings('a').attr('href');
        $(this).siblings('ul').addClass('active');
        burger_header.addClass('active');
        burger_header.find('.conteneur-rappel-menu').html('<a class="rappel-menu"></a>');
        burger_header.find('.rappel-menu').html(nomParent);
        burger_header.find('.rappel-menu').attr('href', hrefParent);
    });

    // Bouton "retour"
    $('.retour-burger').on('click', function () {
        if (burger_ul_3e_nv.hasClass('active')) {
            var nomParent = $('.menu-mobile ul ul.active').parents('li').find('a').html();
            var hrefParent = $('.menu-mobile ul ul.active').parents('li').find('a').attr('href');
            burger_header.find('.rappel-menu').html(nomParent);
            burger_header.find('.rappel-menu').attr('href', hrefParent);
            burger_ul_3e_nv.removeClass('active');
        } else {
            burger_ul.removeClass('active');
            burger_header.removeClass('active');
            burger_header.find('.conteneur-rappel-menu').html('<span class="rappel-menu">Nos rubriques</span>');
        }
    });

    $window.on('resize', function () {
        if ($window.width() > 992) {
            // fermeture du burger
            $('.hamburger').removeClass('is-active');
            burger.removeClass('active');
            burger_ul.removeClass('active');
            burger_header.removeClass('active');
            burger_header.find('.conteneur-rappel-menu').html('<span class="rappel-menu">Nos rubriques</span>');
            body.removeClass('no-scroll');
        }
    });


    /*** FIN MENU BURGER ***/

    /*** RECHERCHE HEADER ***/

    $('.trigger-recherche').on('click', function () {
        $('.wrapper-nav-recherche').toggleClass('recherche-active');
        $(this).toggleClass('recherche-active');
        $('.recherche input[type="text"]').focus();
    });


    /*** FIN RECHERCHE HEADER ***/


    /*** SLICKS ****/
    // mettez ici tous les slicks du site

    // // Home - Slider recherche
    if ($(document).find('.home .slider-images')) {
        $('.home .slider-images').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            prevArrow: $('.conteneur-recherche-home .arrow-prev'),
            nextArrow: $('.conteneur-recherche-home .arrow-next'),
            autoplay: 5000,
            speed: 1000,
            touchMove: false,
            swipe: false,
            draggable: true,
            pauseOnFocus: false,
            pauseOnHover: false,
            fade: true,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        arrows: false
                    }
                }
            ]

        });
    }

    // Home - Slider success stories
    var $sliderSuccessStoriesSettings = {
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: true,
        prevArrow: $('.footer-success-stories .nav-slider .arrow.prev'),
        nextArrow: $('.footer-success-stories .nav-slider .arrow.next'),
        appendDots: $('.footer-success-stories .nav-slider '),
    };
    if ($(document).find('.slider-success-stories')) {
        $('.conteneur-story').each(function (count) {
            if (count < 2) {
                $(this).addClass('groupe-1');
            } else if (count < 4) {
                $(this).addClass('groupe-2');
            } else {
                $(this).addClass('groupe-3');
            }
        });

        if ($window.width() > 991) {
            sliderSuccessStories.slick($sliderSuccessStoriesSettings);
        } else {
            $('.slider-success-stories .conteneur-story').unwrap('.groupe-success-story');
            sliderSuccessStories.slick($sliderSuccessStoriesSettings);
        }
    }

    $window.on('resize', function () {
        if ($(document).find('.slider-success-stories')) {
            if ($window.width() > 991) {
                if ($('.conteneur-story').parent('.groupe-success-story').length > 0) {
                    if (!sliderSuccessStories.hasClass('slick-initialized')) {
                        sliderSuccessStories.slick($sliderSuccessStoriesSettings);
                    }
                } else {
                    if (sliderSuccessStories.hasClass('slick-initialized')) {
                        sliderSuccessStories.slick('unslick');
                    }
                    $('.slider-success-stories .groupe-1').wrapAll('<div class="groupe-success-story"></div>');
                    $('.slider-success-stories .groupe-2').wrapAll('<div class="groupe-success-story"></div>');
                    $('.slider-success-stories .groupe-3').wrapAll('<div class="groupe-success-story"></div>');
                    sliderSuccessStories.slick($sliderSuccessStoriesSettings);

                }
            } else {
                if ($('.conteneur-story').parent('.groupe-success-story').length > 0) {
                    if (sliderSuccessStories.hasClass('slick-initialized')) {
                        sliderSuccessStories.slick('unslick');
                    }
                    $('.slider-success-stories .conteneur-story').unwrap('.groupe-success-story');
                    sliderSuccessStories.slick($sliderSuccessStoriesSettings);
                } else {
                    if (!sliderSuccessStories.hasClass('slick-initialized')) {
                        sliderSuccessStories.slick($sliderSuccessStoriesSettings);
                    }


                }
            }
        }
    });


    // Home - Slider actualités
    if ($(document).find('.actualites-evenements .slider-img-actu')) {
        var sectionActuWidth = $('.dernieres-actualites').width();
        var tailleMargeGauche = ($(window).width() - $('.container').width()) / 2;
        var tailleSlider = sectionActuWidth + tailleMargeGauche;
        $('.slider-img-actu').width(tailleSlider);
        $('.actualites-evenements .slider-img-actu').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            touchMove: false,
            swipe: false,
            draggable: true,
            pauseOnFocus: false,
            pauseOnHover: false,
            fade: true,
            asNavFor: '.slider-actualites'
        });
        $('.actualites-evenements .slider-actualites').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: '.slider-img-actu',
            dots: true,
            prevArrow: $('.footer-dernieres-actualites .nav-slider .arrow.prev'),
            nextArrow: $('.footer-dernieres-actualites .nav-slider .arrow.next'),
            appendDots: $('.footer-dernieres-actualites .nav-slider '),
        });
    }


    /*** Slickification > Home - prestations ***/
    // on load (ne pas oublier le resize)
    if ($(document).find('.home .conteneur-prestations')) {
        if ($(window).width() < 992) {
            $('.home .conteneur-prestations').slick({
                dots: true,
                arrows: true,
                infinite: true,
                slidesToScroll: 2,
                slidesToShow: 2,
                autoplay: 4000,
                speed: 300,
                prevArrow: $('.footer-prestations .nav-slider .arrow.prev'),
                nextArrow: $('.footer-prestations .nav-slider .arrow.next'),
                appendDots: $('.footer-prestations .nav-slider '),
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToScroll: 1,
                            slidesToShow: 1,
                        }
                    }
                ]
            });
        }
        if ($(window).width() > 992) {
            if ($('.home .conteneur-prestations').hasClass('slick-initialized')) {
                $('.home .conteneur-prestations').slick('unslick');
            }
        }
    }

    // gestion du resize (ne pas oublier le on load)
    $(window).on('resize', function () {
        if ($(window).width() < 992 && $('.home .conteneur-prestations').length > 0) {
            if (!$('.home .conteneur-prestations').hasClass('slick-initialized')) {
                $('.home .conteneur-prestations').slick({
                    dots: true,
                    arrows: true,
                    infinite: true,
                    slidesToScroll: 2,
                    slidesToShow: 2,
                    autoplay: 4000,
                    speed: 300,
                    prevArrow: $('.footer-prestations .nav-slider .arrow.prev'),
                    nextArrow: $('.footer-prestations .nav-slider .arrow.next'),
                    appendDots: $('.footer-prestations .nav-slider '),
                    responsive: [
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToScroll: 1,
                                slidesToShow: 1,
                            }
                        }
                    ]
                });
            }
        }

        if ($(window).width() > 992 && $(document).find('.home .conteneur-prestations')) {
            if ($('.home .conteneur-prestations').hasClass('slick-initialized')) {
                $('.home .conteneur-prestations').slick('unslick');
            }
        }
    });

    /*** Slickification > Home - ressources documentataires ***/
    // on load (ne pas oublier le resize)
    if ($(document).find('.home .liste-ressources')) {
        if ($(window).width() < 992) {
            $('.home .liste-ressources').slick({
                dots: true,
                arrows: true,
                infinite: true,
                slidesToScroll: 2,
                slidesToShow: 2,
                autoplay: 4000,
                speed: 300,
                prevArrow: $('.footer-ressources .nav-slider .arrow.prev'),
                nextArrow: $('.footer-ressources .nav-slider .arrow.next'),
                appendDots: $('.footer-ressources .nav-slider '),
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToScroll: 1,
                            slidesToShow: 1,
                        }
                    }
                ]
            });
        }
        if ($(window).width() > 992) {
            if ($('.home .liste-ressources').hasClass('slick-initialized')) {
                $('.home .liste-ressources').slick('unslick');
            }
        }
    }

    // gestion du resize (ne pas oublier le on load)
    $(window).on('resize', function () {
        if ($(window).width() < 992 && $('.home .liste-ressources').length > 0) {
            if (!$('.home .liste-ressources').hasClass('slick-initialized')) {
                $('.home .liste-ressources').slick({
                    dots: true,
                    arrows: true,
                    infinite: true,
                    slidesToScroll: 2,
                    slidesToShow: 2,
                    autoplay: 4000,
                    speed: 300,
                    prevArrow: $('.footer-ressources .nav-slider .arrow.prev'),
                    nextArrow: $('.footer-ressources .nav-slider .arrow.next'),
                    appendDots: $('.footer-ressources .nav-slider '),
                    responsive: [
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToScroll: 1,
                                slidesToShow: 1,
                            }
                        }
                    ]
                });
            }
        }

        if ($(window).width() > 992 && $(document).find('.home .liste-ressources')) {
            if ($('.home .liste-ressources').hasClass('slick-initialized')) {
                $('.home .liste-ressources').slick('unslick');
            }
        }
    });

    /*** Slickification > Home - secteurs d'activités ***/
    // on load (ne pas oublier le resize)
    if ($(document).find('.home .conteneur-secteurs')) {
        if ($(window).width() < 992) {
            $('.home .conteneur-secteurs').slick({
                dots: true,
                arrows: true,
                infinite: true,
                slidesToScroll: 2,
                slidesToShow: 2,
                autoplay: 4000,
                speed: 300,
                prevArrow: $('.footer-secteurs .nav-slider .arrow.prev'),
                nextArrow: $('.footer-secteurs .nav-slider .arrow.next'),
                appendDots: $('.footer-secteurs .nav-slider '),
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToScroll: 1,
                            slidesToShow: 1,
                        }
                    }
                ]
            });
        }
        if ($(window).width() > 992) {
            if ($('.home .conteneur-secteurs').hasClass('slick-initialized')) {
                $('.home .conteneur-secteurs').slick('unslick');
            }
        }
    }

    // gestion du resize (ne pas oublier le on load)
    $(window).on('resize', function () {
        if ($(window).width() < 992 && $('.home .conteneur-secteurs').length > 0) {
            if (!$('.home .conteneur-secteurs').hasClass('slick-initialized')) {
                $('.home .conteneur-secteurs').slick({
                    dots: true,
                    arrows: true,
                    infinite: true,
                    slidesToScroll: 2,
                    slidesToShow: 2,
                    autoplay: 4000,
                    speed: 300,
                    prevArrow: $('.footer-secteurs .nav-slider .arrow.prev'),
                    nextArrow: $('.footer-secteurs .nav-slider .arrow.next'),
                    appendDots: $('.footer-secteurs .nav-slider '),
                    responsive: [
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToScroll: 1,
                                slidesToShow: 1,
                            }
                        }
                    ]
                });
            }
        }

        if ($(window).width() > 992 && $(document).find('.home .conteneur-secteurs')) {
            if ($('.home .conteneur-secteurs').hasClass('slick-initialized')) {
                $('.home .conteneur-secteurs').slick('unslick');
            }
        }
    });

    /*** Slickification > Page secteur - Recherche ***/
    // on load (ne pas oublier le resize)
    if ($(document).find('.page-secteur .projets-recherche .conteneur-projets')) {
        if ($(window).width() < 1261) {
            $('.page-secteur .projets-recherche .conteneur-projets').slick({
                dots: true,
                arrows: true,
                infinite: true,
                slidesToScroll: 2,
                slidesToShow: 2,
                autoplay: 4000,
                speed: 300,
                prevArrow: $('.footer-recherche .nav-slider .arrow.prev'),
                nextArrow: $('.footer-recherche .nav-slider .arrow.next'),
                appendDots: $('.footer-recherche .nav-slider '),
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToScroll: 1,
                            slidesToShow: 1,
                        }
                    }
                ]
            });
        }
        if ($(window).width() > 1260) {
            if ($('.page-secteur .projets-recherche .conteneur-projets').hasClass('slick-initialized')) {
                $('.page-secteur .projets-recherche .conteneur-projets').slick('unslick');
            }
        }
    }

    // gestion du resize (ne pas oublier le on load)
    $(window).on('resize', function () {
        if ($(window).width() < 1261 && $('.page-secteur .projets-recherche .conteneur-projets').length > 0) {
            if (!$('.page-secteur .projets-recherche .conteneur-projets').hasClass('slick-initialized')) {
                $('.page-secteur .projets-recherche .conteneur-projets').slick({
                    dots: true,
                    arrows: true,
                    infinite: true,
                    slidesToScroll: 2,
                    slidesToShow: 2,
                    autoplay: 4000,
                    speed: 300,
                    prevArrow: $('.footer-recherche .nav-slider .arrow.prev'),
                    nextArrow: $('.footer-recherche .nav-slider .arrow.next'),
                    appendDots: $('.footer-recherche .nav-slider '),
                    responsive: [
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToScroll: 1,
                                slidesToShow: 1,
                            }
                        }
                    ]
                });
            }
        }

        if ($(window).width() > 1260 && $(document).find('.page-secteur .projets-recherche .conteneur-projets')) {
            if ($('.page-secteur .projets-recherche .conteneur-projets').hasClass('slick-initialized')) {
                $('.page-secteur .projets-recherche .conteneur-projets').slick('unslick');
            }
        }
    });

    /*** Slickification > Page secteur - Réalisations ***/
    // on load (ne pas oublier le resize)
    if ($(document).find('.page-secteur .section-realisations .conteneur-realisations')) {
        if ($(window).width() < 1261) {
            $('.page-secteur .section-realisations .conteneur-realisations').slick({
                dots: true,
                arrows: true,
                infinite: true,
                slidesToScroll: 2,
                slidesToShow: 2,
                autoplay: 4000,
                speed: 300,
                prevArrow: $('.footer-realisations .nav-slider .arrow.prev'),
                nextArrow: $('.footer-realisations .nav-slider .arrow.next'),
                appendDots: $('.footer-realisations .nav-slider '),
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToScroll: 1,
                            slidesToShow: 1,
                        }
                    }
                ]
            });
        }
        if ($(window).width() > 1260) {
            if ($('.page-secteur .section-realisations .conteneur-realisations').hasClass('slick-initialized')) {
                $('.page-secteur .section-realisations .conteneur-realisations').slick('unslick');
            }
        }
    }

    // gestion du resize (ne pas oublier le on load)
    $(window).on('resize', function () {
        if ($(window).width() < 1261 && $('.page-secteur .section-realisations .conteneur-realisations').length > 0) {
            if (!$('.page-secteur .section-realisations .conteneur-realisations').hasClass('slick-initialized')) {
                $('.page-secteur .section-realisations .conteneur-realisations').slick({
                    dots: true,
                    arrows: true,
                    infinite: true,
                    slidesToScroll: 2,
                    slidesToShow: 2,
                    autoplay: 4000,
                    speed: 300,
                    prevArrow: $('.footer-realisations .nav-slider .arrow.prev'),
                    nextArrow: $('.footer-realisations .nav-slider .arrow.next'),
                    appendDots: $('.footer-realisations .nav-slider '),
                    responsive: [
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToScroll: 1,
                                slidesToShow: 1,
                            }
                        }
                    ]
                });
            }
        }

        if ($(window).width() > 1260 && $(document).find('.page-secteur .section-realisations .conteneur-realisations')) {
            if ($('.page-secteur .section-realisations .conteneur-realisations').hasClass('slick-initialized')) {
                $('.page-secteur .section-realisations .conteneur-realisations').slick('unslick');
            }
        }
    });
    /*** Slickification > Page prestations - Retours d'expérience ***/
    // on load (ne pas oublier le resize)
    if ($(document).find('.page-prestation .section-realisations .conteneur-realisations')) {
        if ($(window).width() < 1261) {
            $('.page-prestation .section-realisations .conteneur-realisations').slick({
                dots: true,
                arrows: true,
                infinite: true,
                slidesToScroll: 2,
                slidesToShow: 2,
                autoplay: 4000,
                speed: 300,
                prevArrow: $('.footer-realisations .nav-slider .arrow.prev'),
                nextArrow: $('.footer-realisations .nav-slider .arrow.next'),
                appendDots: $('.footer-realisations .nav-slider '),
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToScroll: 1,
                            slidesToShow: 1,
                        }
                    }
                ]
            });
        }
        if ($(window).width() > 1260) {
            if ($('.page-prestation .section-realisations .conteneur-realisations').hasClass('slick-initialized')) {
                $('.page-prestation .section-realisations .conteneur-realisations').slick('unslick');
            }
        }
    }

    // gestion du resize (ne pas oublier le on load)
    $(window).on('resize', function () {
        if ($(window).width() < 1261 && $('.page-prestation .section-realisations .conteneur-realisations').length > 0) {
            if (!$('.page-prestation .section-realisations .conteneur-realisations').hasClass('slick-initialized')) {
                $('.page-prestation .section-realisations .conteneur-realisations').slick({
                    dots: true,
                    arrows: true,
                    infinite: true,
                    slidesToScroll: 2,
                    slidesToShow: 2,
                    autoplay: 4000,
                    speed: 300,
                    prevArrow: $('.footer-realisations .nav-slider .arrow.prev'),
                    nextArrow: $('.footer-realisations .nav-slider .arrow.next'),
                    appendDots: $('.footer-realisations .nav-slider '),
                    responsive: [
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToScroll: 1,
                                slidesToShow: 1,
                            }
                        }
                    ]
                });
            }
        }

        if ($(window).width() > 1260 && $(document).find('.page-prestation .section-realisations .conteneur-realisations')) {
            if ($('.page-prestation .section-realisations .conteneur-realisations').hasClass('slick-initialized')) {
                $('.page-prestation .section-realisations .conteneur-realisations').slick('unslick');
            }
        }
    });


    /*** Slickification > Liste - Success Stories ***/
    // on load (ne pas oublier le resize)
    if ($(document).find('.liste .groupe-success-story')) {
        if ($(window).width() < 992) {
            $('.liste .groupe-success-story').slick({
                dots: true,
                arrows: true,
                infinite: true,
                slidesToScroll: 1,
                slidesToShow: 1,
                autoplay: 4000,
                speed: 300,
                prevArrow: $('.footer-success-stories .nav-slider .arrow.prev'),
                nextArrow: $('.footer-success-stories .nav-slider .arrow.next'),
                appendDots: $('.footer-success-stories .nav-slider '),
            });
        }
        if ($(window).width() > 991) {
            if ($('.liste .groupe-success-story').hasClass('slick-initialized')) {
                $('.liste .groupe-success-story').slick('unslick');
            }
        }
    }

    // gestion du resize (ne pas oublier le on load)
    $(window).on('resize', function () {
        if ($(window).width() < 992 && $('.liste .groupe-success-story').length > 0) {
            if (!$('.liste .groupe-success-story').hasClass('slick-initialized')) {
                $('.liste .groupe-success-story').slick({
                    dots: true,
                    arrows: true,
                    infinite: true,
                    slidesToScroll: 1,
                    slidesToShow: 1,
                    autoplay: 4000,
                    speed: 300,
                    prevArrow: $('.footer-success-stories .nav-slider .arrow.prev'),
                    nextArrow: $('.footer-success-stories .nav-slider .arrow.next'),
                    appendDots: $('.footer-success-stories .nav-slider '),
                });
            }
        }

        if ($(window).width() > 991 && $(document).find('.liste .groupe-success-story')) {
            if ($('.liste .groupe-success-story').hasClass('slick-initialized')) {
                $('.liste .groupe-success-story').slick('unslick');
            }
        }
    });

    /*** FIN SLICKS ***/

    /*** SPECIFIQUE A LA HOME ***/

    if ($document.find('.slider-img-actu') && $window.width() < 992) {
        var actualiteSlider = $('.dernieres-actualites').height();
        $('.slider-img-actu').height(actualiteSlider);
    }

    $window.on('resize', function () {
        if ($document.find('.slider-img-actu') && $window.width() < 992) {
            var actualiteSlider = $('.dernieres-actualites').height();
            $('.slider-img-actu').height(actualiteSlider);
        } else if ($document.find('.slider-img-actu') && $window.width() > 991) {
            $('.slider-img-actu').height('calc(100% - 75px)');
        }
    });

    if ($document.find('.conteneur-prestations') && $window.width() > 991) {
        $('.conteneur-prestation').each(function () {
            $(this).find('.conteneur-detail').height($(this).find('.titre').height() + 20);
        });
        $('.prestation').on('mouseover', function () {
            var hauteur = $(this).find('.titre').height() + $(this).find('.overlay').height() + 30;
            $(this).find('.conteneur-detail').height(hauteur);
        });
        $('.prestation').on('mouseleave', function () {
            $(this).find('.conteneur-detail').height($(this).find('.titre').height() + 20);
        });
    }

    /*** FIN SPECIFIQUE A LA HOME ***/


    /*** SPECIFIQUE WYSIWYG ***/

    if ($document.find('.elementor').length > 0) {
        $('.wysiwyg').addClass('contenu-elementor');
    }


    /*** FIN SPECIFIQUE WYSIWYG ***/


    /*** SELECT 2 ***/

    if ($document.find('.liste-ressources-documentaires').length > 0) {
        $('#document').select2({
            placeholder: "Document(s)",
        });
        $('#secteur').select2({
            placeholder: "Secteur(s)",
        });
        $('#mot').select2({
            placeholder: "Mot(s)-clé(s)",
        });
    }
    if ($document.find('.liste-evenements').length > 0) {
        $('#categorie').select2({
            placeholder: "Catégorie(s)",
        });
        $('#secteur').select2({
            placeholder: "Secteur(s)",
        });
        $('#mois').select2({
            placeholder: "Mois",
        });
    }
    if ($document.find('.liste-actualites').length > 0) {
        $('#categorie').select2({
            placeholder: "Catégorie(s)",
        });
        $('#secteur').select2({
            placeholder: "Secteur(s)",
        });
    }
    if ($document.find('.liste-success-stories').length > 0) {
        $('#secteur').select2({
            placeholder: "Secteur(s)",
        });
    }
    if ($document.find('.liste-certifications').length > 0) {
        $('#secteur').select2({
            placeholder: "Secteur(s)",
        });
    }
    if ($document.find('.liste-projets').length > 0) {
        $('#secteur').select2({
            placeholder: "Secteur(s)",
        });
        $('#mot').select2({
            placeholder: "Mot(s)-clé(s)",
        });
    }

    /*** FIN SELECT 2 ***/

    /*** LISTE DES EVENEMENTS - CALENDAR ***/

    if($document.find('.liste-evenements').length > 0) {
        //$("#calendrier").simpleCalendar();
        $("#calendrier").simpleCalendar({
            //Defaults options below
            //string of months starting from january
            months: ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
            days: ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'], //string of days starting from sunday
            minDate : "DD--MM-YYYY",         // minimum date
            maxDate : "DD--MM-YYYY",         // maximum date
            insertEvent: false,              // can insert events
            displayYear: true,              // Display year in header
            //fixedStartDay: true,            // Week begin always by monday
            displayEvent: true,             // Display existing event
            fixedStartDay: false,
            disableEmptyDetails: true,
            events: [
                // generate new event after tomorrow for one hour
                {
                    startDate: new Date(new Date().setHours(new Date().getHours() + 24)).toDateString(),
                    endDate: new Date(new Date().setHours(new Date().getHours() + 25)).toISOString(),
                    summary: 'Visit of the Eiffel Tower'
                },
                // generate new event for yesterday at noon
                {
                    startDate: new Date(new Date().setHours(new Date().getHours() - new Date().getHours() - 12, 0)).toISOString(),
                    endDate: new Date(new Date().setHours(new Date().getHours() - new Date().getHours() - 11)).getTime(),
                    summary: 'Restaurant'
                },
                // generate new event for the last two days
                {
                    startDate: new Date(new Date().setHours(new Date().getHours() - 48)).toISOString(),
                    endDate: new Date(new Date().setHours(new Date().getHours() - 24)).getTime(),
                    summary: 'Visit of the Louvre'
                }
            ],                     // List of events
            onInit: function (calendar) {}, // Callback after first initialization
            onMonthChange: function (month, year) {}, // Callback on month change
            onDateSelect: function (date, events) {} // Callback on date selection
        });
    }

    /*** FIN LISTE DES EVENEMENTS - CALENDAR ***/


});

